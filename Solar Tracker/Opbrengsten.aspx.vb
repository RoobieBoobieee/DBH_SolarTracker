﻿Imports System.IO.Ports
Partial Class Opbrengsten
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'txtSpanning.Text = Update("1")
        'txtStroom.Text = Update(2)
        'txtVermogen.Text = Update(3)
        txtVerhLedsHor.Text = Update("4")
        txtVerhLedsVer.Text = Update("5")
        txtStandServo.Text = Update("6")
        txtStandStepper.Text = Update("7")

    End Sub
    Public Function Update(ByVal Gegevens As String) As String
        Dim Com3 As New IO.Ports.SerialPort
        With Com3
            .PortName = "COM3"
            .BaudRate = 9600
            .DataBits = 8
            .StopBits = StopBits.One
            .Parity = Parity.None
            .ReadTimeout = 50 '=5sec

            .DtrEnable = False
            .RtsEnable = False

            .NewLine = Chr(13)

        End With
        Dim Incoming As String = ""
        Dim tmp As String = ""
        Try
            Com3.Close()
            Com3.Open()



            Do
                Try
                    Do While Incoming = "" Or Incoming = "0"
                        Try
                            Com3.WriteLine(Gegevens & vbNewLine)

                            Incoming = Com3.ReadLine()
                        Catch
                        End Try

                    Loop

                    If Incoming = Nothing Then

                    Else
                        Com3.Close()
                        Return Incoming

                        Exit Do

                    End If
                Catch ex As TimeoutException
                    Com3.Close()
                    Return ("Failed To Load (Timed Out)")

                End Try

            Loop

                Com3.Close()
        Catch ex As Exception
            Com3.Close()
        End Try
        Return ("Er is een onbekende fout opgetreden")

    End Function

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Page_Load(sender, e)
    End Sub
End Class
